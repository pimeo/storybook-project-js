# Storybook project JS

## Storybook installation
- https://freesoft.dev/program/163707737  

## Resources
- https://github.com/storybookjs/storybook/tree/master

## Addons
- https://github.com/storybookjs/storybook/tree/master/addons

## Addons installed
- https://github.com/storybookjs/storybook/tree/master/addons/actions  
Storybook Addon Actions can be used to display data received by event handlers in Storybook.  

- https://github.com/storybookjs/storybook/tree/master/addons/centered
Centered components in canvas  

- https://github.com/storybookjs/storybook/tree/master/addons/a11y
This storybook addon can be helpful to make your UI components more accessible.  

- https://github.com/storybookjs/storybook/tree/master/addons/knobs
Storybook Addon Knobs allow you to edit React props dynamically using the Storybook UI

- https://github.com/storybookjs/storybook/tree/master/addons/backgrounds
Change canvas background

- https://github.com/storybookjs/storybook/tree/master/addons/links
Add internal links

- https://github.com/tuchk4/storybook-readme
Add a readme a the current story

- https://github.com/storybookjs/storybook/tree/master/addons/viewport
Show story in a custom viewport size


## Inspirations

- https://storybooks-vue.netlify.com/?path=/story/welcome--welcome
- https://reactivesearch-vue-playground.netlify.com/?path=/story/range-components-singlerange--basic
- https://github.com/appbaseio/vue-playground/blob/master/.storybook/addons.js
- https://storybooks-official.netlify.com/?path=/story/addons-jest--withtests