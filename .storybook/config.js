// References
// https://storybook.js.org/docs/guides/guide-vue/

import { addParameters, addDecorator, configure } from '@storybook/vue'
import { addReadme } from 'storybook-readme/vue'

//
// vue
//

import Vue from 'vue'

//
// Load components
//

const modal_test = require('./../src/components/modal-test/modal-test.js')
Vue.component('modal-test', modal_test)

//
// global storybook parameters
// 

addParameters({
  options: {
    panelPosition: 'bottom'
  }
})

//
// global storybook decorators
//

addDecorator(addReadme)

//
// Load stories
//

const loadStories = () => {
  const req = require.context('./../src/components', true, /\.stories\.js$/)
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)