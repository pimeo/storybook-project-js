import '@storybook/addon-actions/register'
import '@storybook/addon-a11y/register'
import '@storybook/addon-knobs/register'
import '@storybook/addon-backgrounds/register'
import '@storybook/addon-storysource/register'
import '@storybook/addon-viewport/register'
import 'storybook-readme/register'

// register notes addon as a tab
// import '@storybook/addon-notes/register'
// or register notes addon as a panel (once one can be used)
import '@storybook/addon-notes/register-panel'


// tests
import '@storybook/addon-jest/register';