const Vue = require("vue")


document.addEventListener('DOMContentLoaded', () => {
  new Vue({
    el: '#app',
    mounted: () => {
      console.log('vue app mounted')
    }
  })
}, false)