import { storiesOf } from "@storybook/vue"
import { action, actions } from "@storybook/addon-actions"

storiesOf('Addons', module)
  .add('action only', () => ({
    template: '<button @click.prevent="log" @dblclick.prevent="logdbclick">Action button</button>',
    methods: {
      log: action('log click'),
      logdbclick: (e) => {
        e.preventDefault()
        action('log dbclick')(e.target)
      }
    }
  })
  )