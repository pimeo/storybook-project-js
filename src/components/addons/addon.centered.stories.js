import { storiesOf } from "@storybook/vue"
import Centered from '@storybook/addon-centered/vue'

storiesOf('Addons', module)
.addDecorator(Centered)
.add('centered', () => "<button>Centered button</button>")