import { storiesOf } from '@storybook/vue';

storiesOf('Addons', module)
  .add(
    'Simple note',
    () => ({
      template:
        '<p><strong>Etiam vulputate elit eu venenatis eleifend. Duis nec lectus augue. Morbi egestas diam sed vulputate mollis. Fusce egestas pretium vehicula. Integer sed neque diam. Donec consectetur velit vitae enim varius, ut placerat arcu imperdiet. Praesent sed faucibus arcu. Nullam sit amet nibh a enim eleifend rhoncus. Donec pretium elementum leo at fermentum. Nulla sollicitudin, mauris quis semper tempus, sem metus tristique diam, efficitur pulvinar mi urna id urna.</strong></p>',
    }),
    { notes: 'Notes particulières sur le texte en gras ?' }
  )
  .add(
    'Note with HTML',
    () => ({
      template: `
      <div>
        <h1>Composant : titre</h1>
        <p>Composant: description avec contenu</p>
      </div>

      `,
    }),
    {
      notes: `
      <h2>Version HTML</h2>

      <em>It's not all that important to be honest, but..</em>

    `,
    }
  );