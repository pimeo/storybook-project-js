import { storiesOf } from "@storybook/vue"
import { withTests } from "@storybook/addon-jest"

import jest_results from "./../../../.jest-test-results.json"

// https://github.com/storybookjs/storybook/tree/master/addons/jest#usage
storiesOf('Addons', module)
  .addDecorator(
    withTests({ results: jest_results })
  )
  .add(
    'jest test 1 results',
    () => "<div>Jest results in storybook from src/addons/tests/one.test.js</div>",
    {
      jest: ['one.test.js']
    }
  )