import { storiesOf } from '@storybook/vue';
import { linkTo } from '@storybook/addon-links';

storiesOf('Addons', module)
  .add('Go to welcome', () => ({
    template:
      '<button @click.prevent="redirect_to">Clic pour rediriger vers la page Modal Test</button>',
    methods: {
      redirect_to: linkTo('Modal Test'),
    },
  }));