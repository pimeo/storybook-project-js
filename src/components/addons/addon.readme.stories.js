import { storiesOf } from '@storybook/vue';

import readme from './addon.readme.md'

storiesOf('Addons', module)
  .addParameters({
    readme: {
      // show readme before story
      // content: readme,
      // show readme at the addons panel
      sidebar: readme
    }
  })
  .add('Readme contents', () => '<p>Readme contents in Readme panel</p>')