// storybook
import { storiesOf } from '@storybook/vue'

// create the story
storiesOf('Modal Test', module)
// .addParameters({
//   options: {
//     option_1: true,
//     option_2: 'bottom',
//     option_3: 145,
//     theme: undefined
//   }
// })
.add('with html text', () => '<button>This is a button</button>')
.add(
  'with vue component', 
  () => '<modal-test></modal-test>',
  { 
    options: {
      option_2: 'left'
    }
  }
)

// .add('with vue component', () => ({
//   components: {
//     'modal-test': modal_test
//   },
//   template: '<modal-test>coucou</modal-test>'
// }))